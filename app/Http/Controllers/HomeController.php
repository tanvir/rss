<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SimpleXMLElement;
use View;

use vendor\willvincent\feeds\src\FeedsServiceProvider\Feed;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $content = file_get_contents('https://www.tagesschau.de/xml/rss2');
        // $flux = new \SimpleXMLElement($content);
        // //echo $flux;
        // return view('frontEnd/homepage', compact('flux'));


            $feed = \Feeds::make('https://www.tagesschau.de/xml/rss2');
            $data = array(
              'title'     => $feed->get_title(),
              'permalink' => $feed->get_permalink(),
              'items'     => $feed->get_items(),
               // 'image' =>    $feed->get_image('img', 0),
               // 'image_url' => $feed->$image->src
            );

            return View::make('frontEnd/homepage', $data);
    }
}

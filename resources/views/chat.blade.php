<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Chat Application</title>
	<style>
		.list-group{
			overflow-y: scroll;
			height: 200px;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row" id="app">
			<div class="offset-6 col-4 col-sm-8 offset-sm-1">
			<li class="list-group-item active">Chat Room</li>
			<div class="badge badge-pill badge-primary">@{{typing}}</div>
			 <ul class="list-group" v-chat-scroll>
			  
			  <message 
			  v-for="value,index in chat.message"
			  :key=value.index
			  :color=chat.color[index]
			  :user=chat.user[index]
			  >
			  	@{{ value }}
			  </message>
			 
			</ul>
			 <input type="text" class="form-control" placeholder="Type you message here...." v-model='message' v-on:keyup.enter='send'>
			</div>
		</div>
	</div>
	<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
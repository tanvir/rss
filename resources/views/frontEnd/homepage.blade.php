<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Rss Feed</title>
</head>
<body>
	<home>
		<div class="container" id="app">
		<div class="row">

		   <div class="header">
		    <h1><a href="{{ $permalink }}">{{ $title }}</a></h1>
		    
		  </div>

		  @foreach ($items as $item)
		    <div class="item">
		      <h2><a href="{{ $item->get_permalink() }}">{{ $item->get_title() }}</a></h2>
		      <p>{{ $item->get_description() }}</p>
			   
		      <p><small>Posted on {{ $item->get_date('j F Y | g:i a') }}</small></p>
		    </div>

			@endforeach
		</div>
	</div>
	</home>
	<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('chat');
});

//Route::get('/rss','homeController@rss');

Route::get('/rss', 'HomeController@index');

// Route::get('/chat','chatController@chat');
// Route::post('/send','chatController@send');

// Auth::routes();


